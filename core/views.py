from django.shortcuts import render, get_object_or_404, redirect
from .forms import ProfileForm
from django.contrib.auth import authenticate, login, logout
from .models import Post
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, View, DetailView


# Create your views here.

def index(request):
    posts = Post.objects.all()[1:]
    latest = Post.objects.all()[0]
    context = {
        'posts' : posts,
        'latest' : latest
    }
    return render(request,'core/index.html', context)

class PostView(DetailView):
    model = Post
    context_object_name = 'post'
    template_name = 'core/post.html'
    

def verify(request):
    return render(request, 'core/verify.html')

from django.core.mail import send_mail
from magazine.settings import EMAIL_HOST_USER
from django.template.loader import render_to_string
from django.utils.html import strip_tags

def register(request):
    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if form.is_valid():
            user = form.save()
            user = request.user
            new_user = authenticate(username=form.cleaned_data['username'],
                password=form.cleaned_data['password1'],
            )
            login(request, new_user)
            email = form.cleaned_data.get('email')
            subject = 'Hello there!'
            message = 'Thank you for signing up to the OSC School Magazine'
            recipient = email
            username = form.cleaned_data.get('username')
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            act_key = user.profile.activation_key
            x = {
                'user':request.user,
                'key':act_key
            }
            html_message = render_to_string('core/email.html', x)
            plain_message = strip_tags(html_message)
            send_mail(subject, plain_message, EMAIL_HOST_USER, [recipient], html_message=html_message, fail_silently=False)
            print(f'Key is: {act_key}')
            return redirect('core:verify')
    else:
        form = ProfileForm()
    return render(request,'core/register.html', {'form' : form})

def logout_user(request):
    logout(request)
    return redirect('core:index')

from django.http import JsonResponse
from django.contrib.auth.models import User

def validate_username(request):
    username = request.GET.get('username', None)
    data = {
        'is_taken': User.objects.filter(username__iexact=username).exists(),
        'exists': User.objects.filter(username__iexact=username).exists()
    }
    if data['is_taken']:
        data['message'] = 'Someone with that username already exists!'
    elif data['exists'] == False:
        data['message'] = "That username doesn't exist"
    return JsonResponse(data)