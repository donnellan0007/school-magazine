from django.contrib import admin
from django.urls import path
from .views import index, register, verify, validate_username, PostView, logout_user
from . import views
from django.contrib.auth import views as auth_views

app_name = "core"

urlpatterns = [
    path('',views.index,name="index"),
    path('register/',views.register,name="register"),
    path('verify/', views.verify, name='verify'),
    path('post/<str:slug>/',views.PostView.as_view(),name="post"),
    path('logout/',views.logout_user,name='logout'),
    path('login/',auth_views.LoginView.as_view(template_name='core/login.html'),name='login'),
    path('ajax/validate_username/',views.validate_username,name='validate_username'),
]
